build:
	docker-compose build --build-arg DOCKER_UID=$(shell id -u)
	docker-compose run backoffice composer install

start:
	docker-compose up -d

stop:
	docker-compose down

shell:
	docker-compose run backoffice bash
