<?php


namespace App\Form;


use FeedIo\Storage\Entity\Feed\Status;
use FeedIo\Storage\Entity\Topic;
use FeedIo\Storage\Repository\TopicRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class FeedType  extends AbstractType
{

    private TopicRepository $repository;

    public function __construct(TopicRepository $repository)
    {
        $this->repository = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $topics = $this->repository->getTopics();
        $topicChoices = [];
        /** @var Topic $topic */
        foreach ($topics as $topic) {
            $topicChoices[$topic->getName()->getDefault()] = $topic->getId()->__toString();
        }

        $builder
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'Pending' => Status::PENDING,
                    'Accepted' => Status::ACCEPTED,
                    'Approved' => Status::APPROVED,
                    'Rejected' => Status::REJECTED,
                ]
            ])
            ->add('language', ChoiceType::class, [
                'choices' => [
                    '' => '',
                    'English' => 'english',
                    'French' => 'french',
                ]
            ])
            ->add('topic', ChoiceType::class, ['choices' => $topicChoices])
            ->add('submit', SubmitType::class)
        ;
    }

}