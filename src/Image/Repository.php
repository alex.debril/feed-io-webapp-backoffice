<?php


namespace App\Image;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Repository
{
    protected Resizer $resizer;

    protected string $host;

    protected string $folder;

    public function __construct(Resizer $resizer, string $host, string $folder){
        $this->resizer = $resizer;
        $this->host = $host;
        $this->folder = $folder;
    }

    public function saveImage(string $filename, UploadedFile $file): string
    {
        $filepath = "{$this->folder}/{$filename}";
        $path = pathinfo($filepath,  PATHINFO_DIRNAME);
        if (! is_dir($path)) {
            mkdir($path, 0755, true);
        }
        $file->move($this->folder, $filename);
        $local = new Image($filepath);
        $this->resizer->resize($local);

        return "{$this->host}/$filename";
    }

}