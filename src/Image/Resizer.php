<?php declare(strict_types=1);


namespace App\Image;


class Resizer
{

    const DEFAULT_TARGET_WIDTH = 320;

    const DEFAULT_TARGET_HEIGHT = 180;

    protected float $ratio;

    protected int $targetWidth;

    protected int $targetHeight;

    public function __construct(
        int $targetWidth = self::DEFAULT_TARGET_WIDTH,
        int $targetHeight = self::DEFAULT_TARGET_HEIGHT
    ){
        $this->setTargetDimensions($targetWidth, $targetHeight);
    }

    public function setTargetDimensions(int $targetWidth, int $targetHeight)
    {
        $this->targetWidth = $targetWidth;
        $this->targetHeight = $targetHeight;

        $this->ratio = $targetWidth / $targetHeight;
    }

    public function getNewDimensions(int $sourceWidth, int $sourceHeight): ResizeDimension
    {
        $sourceRatio = $sourceWidth / $sourceHeight;
        if ($this->ratio > $sourceRatio) {
            return new ResizeDimension($sourceWidth, (int) ($sourceWidth / $this->ratio));
        }
        return new ResizeDimension((int) ($sourceHeight * $this->ratio), $sourceHeight);
    }

    public function resize(Image $image)
    {
        $dim = $this->getNewDimensions($image->getWidth(), $image->getHeight());
        $horizontalCrop = (int) (($image->getWidth() - $dim->getWidth()) /2);
        $verticalCrop = (int) (($image->getHeight() - $dim->getHeight()) /2);

        $source = $image->newResource();
        $destination = \imagecreatetruecolor(self::DEFAULT_TARGET_WIDTH, self::DEFAULT_TARGET_HEIGHT);
        \imagecopyresized(
            $destination,
            $source,
            0,
            0,
            $horizontalCrop,
            $verticalCrop,
            self::DEFAULT_TARGET_WIDTH,
            self::DEFAULT_TARGET_HEIGHT,
            $dim->getWidth(),
            $dim->getHeight()
        );

        \imagejpeg($destination, $image->getUri());
    }
}