<?php declare(strict_types=1);


namespace App\Image;


class Image
{

    protected ?string $content = null;

    protected string $uri;

    protected string $filename;

    protected ?string $extension;

    protected ?int $type = null;

    protected ?int $width = null;

    protected ?int $height = null;

    public function __construct(string $uri)
    {
        $this->setUri($uri);
    }

    public function getContent(): string
    {
        if (is_null($this->content)) {
            $this->content = file_get_contents($this->uri);
        }

        return $this->content;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getWidth(): int
    {
        $this->loadImageSpecs();
        return $this->width;
    }

    public function getHeight(): int
    {
        $this->loadImageSpecs();
        return $this->height;
    }

    public function getType(): int
    {
        $this->loadImageSpecs();
        return $this->type;
    }

    public function newResource()
    {
        switch ($this->getType()) {
            case IMG_JPEG:
                return \imagecreatefromjpeg($this->getUri());
            case IMG_GIF:
                return \imagecreatefromgif($this->getUri());
            case IMG_PNG:
                return \imagecreatefrompng($this->getUri());
            default:
                throw new \RuntimeException('not supported');
        }
    }

    public function setUri(string $uri)
    {
        $pathInfo = pathinfo($uri);
        $this->uri = $uri;
        $this->filename = $pathInfo['basename'];
        $this->extension =  $pathInfo['extension'] ?? null;
    }

    protected function loadImageSpecs()
    {
        if (is_null($this->width)) {
            list($this->width, $this->height, $this->type) = \getimagesize($this->uri);
        }
    }

}