<?php declare(strict_types=1);


namespace App\Controller;


use App\Form\TopicType;
use App\Image\Repository;
use FeedIo\Storage\Entity\Topic;
use FeedIo\Storage\Entity\Translations;
use FeedIo\Storage\Repository\TopicRepository;
use MongoDB\BSON\ObjectId;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;

class TopicController extends AbstractController
{
    private TopicRepository $repository;

    private Repository $imageRepository;

    private SluggerInterface $slugger;

    public function __construct(TopicRepository $repository, Repository $imageRepository, SluggerInterface $slugger)
    {
        $this->repository = $repository;
        $this->imageRepository = $imageRepository;
        $this->slugger = $slugger;
    }

    public function index(): Response
    {
        return $this->render('topics/list.html.twig', [
            'topics' => $this->repository->getTopics()->toArray(),
        ]);
    }

    public function add(Request $request): Response
    {
        $form = $this->createForm(TopicType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $name = new Translations($data['default']);
            $name->set("english", $data["english"]);
            $name->set('french', $data['french']);
            $topic = new Topic();
            $topic->setSlug($this->slugger->slug($data['default'])->toString());
            $topic->setName($name);
            $filename = "{$topic->getSlug()}.jpg";
            $imageUrl = $this->imageRepository->saveImage($filename, $form['image']->getData());
            $topic->setImage($imageUrl);

            $this->repository->save($topic);

            $this->addFlash('success', "feed successfully updated");
            return $this->redirectToRoute('topics');
        }

        return $this->render('topics/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function edit(string $id, Request $request): Response
    {
        $form = $this->createForm(TopicType::class);
        $form->handleRequest($request);
        $topic = $this->repository->findOne(new ObjectId($id));

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $name = new Translations($data['default']);
            $name->set("english", $data["english"]);
            $name->set('french', $data['french']);
            $topic->setSlug($this->slugger->slug($data['default'])->toString());
            $topic->setName($name);
            $filename = "{$topic->getSlug()}.jpg";
            $imageUrl = $this->imageRepository->saveImage($filename, $form['image']->getData());
            $topic->setImage($imageUrl);

            $this->repository->save($topic);

            $this->addFlash('success', "feed successfully updated");
        } else {
            $form->setData([
                'default' => $topic->getName()->getDefault(),
                'english' => $topic->getName()->get("english"),
                'french' => $topic->getName()->get("french"),
            ]);
        }

        return $this->render("topics/edit.html.twig", [
            'topic' => $topic,
            'form' => $form->createView(),
        ]);
    }

}