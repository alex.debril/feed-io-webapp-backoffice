<?php declare(strict_types=1);


namespace App\Controller;


use App\Form\FeedType;
use FeedIo\FeedIo;
use FeedIo\Storage\Entity\Feed;
use FeedIo\Storage\Repository\FeedRepository;
use FeedIo\Storage\Repository\TopicRepository;
use MongoDB\BSON\ObjectId;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FeedController extends AbstractController
{

    private FeedRepository $repository;

    private FeedIo $feedIo;

    public function __construct(
        FeedRepository $repository,
        FeedIo $feedIo
    )
    {
        $this->repository = $repository;
        $this->feedIo = $feedIo;
    }

    public function index(string $status): Response
    {
        $cursor = $this->repository->getFeedsByStatus(strtoupper($status));
        $feeds = $cursor->toArray();

        return $this->render('feeds.html.twig', [
           'status' => $status,
           'statuses' => [
               Feed\Status::PENDING,
               Feed\Status::ACCEPTED,
               Feed\Status::APPROVED,
               Feed\Status::REJECTED,
           ],
           'feeds' => $feeds,
        ]);
    }

    public function edit(string $id, Request $request): Response
    {
        $feed = $this->repository->findOne(new ObjectId($id));
        $form = $this->createForm(FeedType::class, [
            'status' => $feed->getStatus()->getValue(),
            'language' => $feed->getLanguage(),
            'topic' => $feed->getTopicId() ? $feed->getTopicId()->__toString():null,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $feed->setStatus(new Feed\Status($data['status']));
            $feed->setLanguage($data['language']);
            $feed->setTopicId(new ObjectId($data['topic']));
            $this->repository->save($feed);
            $this->addFlash('success', "feed successfully updated");
        }

        return $this->render('feeds/edit.html.twig', [
            'feed' => $feed,
            'form' => $form->createView(),
        ]);
    }

    public function update(Request $request): JsonResponse
    {
        try {
            $data = json_decode($request->getContent());
            $feed = $this->repository->findOne(new ObjectId($data->id));
            $feed->setStatus(new Feed\Status($data->status));
            $this->repository->save($feed);
            return $this->json(['status' => 'ok']);
        } catch (\Exception $e) {
            return $this->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function preview(string $id): Response
    {
        $feed = $this->repository->findOne(new ObjectId($id));
        return $this->render('feeds/preview.html.twig', [
            'feed' => $this->feedIo->read($feed->getUrl())->getFeed(),
        ]);
    }

}